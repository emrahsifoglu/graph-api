<?php

namespace App\Vendor\Collection;

interface CollectionInterface
{

    public function getArrayCopy();

}
