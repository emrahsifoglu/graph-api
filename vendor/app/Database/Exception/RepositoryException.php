<?php

namespace App\Vendor\Database\Exception;

use RuntimeException;

class RepositoryException extends RuntimeException {

}
